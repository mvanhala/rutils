# rutils 0.3.0

* Enable support for rendering to multiple formats in `render_doc`.

* Added a `NEWS.md` file to track changes to the package.

